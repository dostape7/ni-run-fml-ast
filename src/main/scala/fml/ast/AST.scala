package fml.ast

sealed abstract class AST

case class Identifier(id: String) extends AST

case class Integer(value: Int) extends AST

case class Bool(value: Boolean) extends AST

case class Null() extends AST

case class Variable(name: Identifier, value: AST) extends AST

case class AccessVariable(name: Identifier) extends AST

case class AssignVariable(name: Identifier, value: AST) extends AST

case class Array(size: AST, value: AST) extends AST

case class AccessArray(array: AST, index: AST) extends AST

case class AssignArray(array: AST, index: AST, value: AST) extends AST

case class Function(name: Identifier, params: Vector[Identifier], body: AST) extends AST

case class CallFunction(name: Identifier, args: Vector[AST]) extends AST

case class Print(format: String, arguments: Vector[AST]) extends AST

case class Block(block: Vector[AST]) extends AST

case class Top(body: List[AST]) extends AST

case class Loop(guard: AST, body: AST) extends AST

case class Conditional(guard: AST, thenBranch: AST, elseBranch: AST) extends AST

case class Object(ext: AST, members: Vector[AST]) extends AST

case class AssignField(obj: AST, field: Identifier, value: AST) extends AST

case class AccessField(obj: AST, field: Identifier) extends AST

case class CallMethod(obj: AST, name: Identifier, args: Vector[AST]) extends AST