package fml.parser

import fml.ast._
import io.circe.parser
import scala.io.Source
import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import io.circe.syntax._
import cats.syntax.functor._
import shapeless.{Coproduct, Generic}


class JSONParser {

}

class SimpleDecoder {
  def main(input: String): Unit = {
    /*
        implicit def decodeEvent1: Decoder[AST] =
          List[Decoder[AST]](
            Decoder[Integer].widen,
            Decoder[Identifier].widen,
          ).reduceLeft(_ or _)
    */
    implicit def decodeAdtNoDiscr[A, Repr <: Coproduct](implicit gen: Generic.Aux[A, Repr], decodeRepr: Decoder[Repr]): Decoder[A] = decodeRepr.map(gen.from)

    val decodeResult = parser.decode[AST](input)
    decodeResult match {
      case Right(result) => println(result)
      case Left(error) => throw new RuntimeException(s"Could not parse JSON, $error")
    }
  }

  def parse(file :String): Unit = {
    val src = Source.fromFile(file)
    main(src.mkString)
  }

}
