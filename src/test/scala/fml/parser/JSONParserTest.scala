package fml.parser

import utest._

object JSONParserTest extends TestSuite {
  val tests: Tests = Tests {

    val parser = new SimpleDecoder

    test("example function") {
      val file = "/two.json"
      val res = parser.parse(getClass.getResource(file).getFile)
      print(res)
    }
    test("example not_neat function") {
      val file = "/othertwo.json"
      val res = parser.parse(getClass.getResource(file).getFile)
      print(res)
    }
    test("example function 2") {
      val file = "/two_print.json"
      val res = parser.parse(getClass.getResource(file).getFile)
      print(res)
    }
  }

}
